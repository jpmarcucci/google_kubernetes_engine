provider "google" {
	credentials		= ".credentials"
	project			= "ace-world-233111"
	region			= "us-central1"
}

resource "google_container_node_pool" "pool-padrao" {
	count			= "${var.clusterpoolcount}"
	name			= "${var.clusterpoolname}"
	location		= "${var.clusterpoollocation}"
	node_count		= "${var.clusterpoolnodecount}"
	cluster			= "${google_container_cluster.colmeia.name}"

	node_config {
		disk_size_gb	= "${var.clusterpoolconfig_disksize}"
		disk_type	= "${var.clusterpoolconfig_disktype}"
		image_type	= "${var.clusterpoolconfig_image}"
		local_ssd_count = "${var.clusterpoolconfig_localssdcount}"
		machine_type	= "${var.clusterpoolconfig_machinetype}"
		
		service_account = "${var.clusterpoolconfig_serviceaccount}"
	}
	
}

resource "google_container_cluster" "colmeia" {
	name			= "${var.clustername}"
	description		= "Cluster colmeia producao"
	location		= "${var.clusterlocation}"
	network			= "${var.clusternetwork}"
	subnetwork		= "${var.clusternetworksubnet}"
	remove_default_node_pool = true

	node_pool {
		name		= "default-pool"
	}
	
}
