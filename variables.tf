variable "clustername" {
	description	= "Nome do cluster"
	type		= "string"
	default		= "colmeia"
}

variable "clusterlocation" {
	description	= "Local do cluster"
	type		= "string"
	default		= "us-central1-a"
}

variable "clusternetwork" {
	description	= "Rede do cluster"
	type		= "string"
	default		= "default"
}

variable "clusternetworksubnet" {
	description	= "Subnet"
	type		= "string"
	default		= "default"
}

variable "clusternodecount" {
	description	= "Numeros de node do cluster"
	default		= 2
}

variable "clusterpoolcount" {
	description	= "Numeros de cluster pool"
	type		= "string"
	default		= 1
}

variable "clusterpoolname" {
	description	= "Nome do cluster pool"
	type		= "string"
	default		= "pool-padrao"
}

variable "clusterpoollocation" {
	description	= "Localizacao do cluster pool"
	type		= "string"
	default		= "us-central1-a"
}

variable "clusterpoolnodecount" {
	description 	= "Numero de nodes do cluster pool"
	type		= "string"
	default		= 2
}

variable "clusterpoolconfig_disksize" {
	description	= "Tamanho do disco"
	type		= "string"
	default		= 50
}
	
variable "clusterpoolconfig_disktype" {
	description	= "Tipo do disco"
	type		= "string"
	default		= "pd-standard"
}
variable "clusterpoolconfig_image" {
	description	= "Imagem do node"
	type		= "string"
	default		= "COS"
}
variable "clusterpoolconfig_localssdcount" {
	description	= "SSD"
	default		= 0
}
variable "clusterpoolconfig_machinetype" {
	description	= "Tipo da maquina"
	type		= "string"
	default		= "g1-small"
}

variable "clusterpoolconfig_serviceaccount" {
	description	= "conta de servico"
	type		= "string"
	default		= "default"
}
